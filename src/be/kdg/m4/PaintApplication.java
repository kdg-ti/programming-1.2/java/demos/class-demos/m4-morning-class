package be.kdg.m4;

import be.kdg.m4.view.PaintPresenter;
import be.kdg.m4.view.PaintView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class PaintApplication extends Application {
    @Override
    public void start(Stage stage) throws Exception {
        PaintView view = new PaintView();
        PaintPresenter presenter = new PaintPresenter(view);

        Scene scene = new Scene(view);
        stage.setScene(scene);
        stage.setTitle("Paint");
        stage.sizeToScene();
        stage.show();
    }
}
