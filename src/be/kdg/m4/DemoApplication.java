package be.kdg.m4;

import be.kdg.m4.view.MenuPresenter;
import be.kdg.m4.view.MenuView;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class DemoApplication extends Application {
    @Override
    public void start(Stage stage) {
        MenuView view = new MenuView();
        MenuPresenter presenter = new MenuPresenter(view);

        Scene scene = new Scene(view);
        stage.setScene(scene);
        stage.getIcons().add(new Image("/004Charmander.png"));
        stage.setTitle("Demo");
        stage.setWidth(800.0);
        stage.setHeight(600.0);
        stage.show();
    }
}
