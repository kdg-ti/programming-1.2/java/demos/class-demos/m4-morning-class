package be.kdg.m4.view;

import javafx.scene.control.Menu;
import javafx.scene.control.MenuBar;
import javafx.scene.control.MenuItem;
import javafx.scene.control.RadioButton;
import javafx.scene.control.TextField;
import javafx.scene.control.ToggleGroup;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;

public class MenuView extends BorderPane {
    private final Image bulbasaurImage;

    private MenuItem openItem;
    private MenuItem exitItem;

    private RadioButton charmander;
    private RadioButton bulbasaur;
    private RadioButton squirtle;

    private ImageView pokemonView;

    private TextField userInput;

    public MenuView() {
        bulbasaurImage = new Image("/001Bulbasaur.png");

        initiaseNodes();
        layoutNodes();
    }

    private void initiaseNodes() {
        openItem = new MenuItem("Open");
        exitItem = new MenuItem("Exit");
        openItem.setGraphic(new ImageView(bulbasaurImage));

        charmander = new RadioButton("Charmander");
        bulbasaur = new RadioButton("Bulbasaur");
        squirtle = new RadioButton("Squirtle");

        ToggleGroup pokemonGroup = new ToggleGroup();
        charmander.setToggleGroup(pokemonGroup);
        bulbasaur.setToggleGroup(pokemonGroup);
        squirtle.setToggleGroup(pokemonGroup);

        /*Image image = new Image("/001Bulbasaur.png");
        pokemonView = new ImageView(image);*/
        pokemonView = new ImageView(bulbasaurImage);

        userInput = new TextField(/*"initial text"*/);
    }

    private void layoutNodes() {
        Menu fileMenu = new Menu("File");
        fileMenu.getItems().addAll(openItem, exitItem);
        fileMenu.setGraphic(new ImageView(bulbasaurImage));
        MenuBar menuBar = new MenuBar(fileMenu);
        //menuBar.getMenus().addAll(fileMenu);

        setTop(menuBar);

        setLeft(charmander);
        //setCenter(bulbasaur);
        setRight(squirtle);

        setBottom(pokemonView);

        setCenter(userInput);
    }

    MenuItem getOpenItem() {
        return openItem;
    }

    MenuItem getExitItem() {
        return exitItem;
    }

    TextField getUserInput() {
        return userInput;
    }

    RadioButton getCharmander() {
        return charmander;
    }

    RadioButton getSquirtle() {
        return squirtle;
    }

    ImageView getPokemonView() {
        return pokemonView;
    }
}
