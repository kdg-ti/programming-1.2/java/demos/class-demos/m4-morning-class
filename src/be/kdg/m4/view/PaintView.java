package be.kdg.m4.view;

import javafx.geometry.Insets;
import javafx.scene.canvas.Canvas;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;

public class PaintView extends BorderPane {
    private Canvas canvas;
    private Label statusLabel;

    private static final double DOT_RADIUS = 1.0;

    public PaintView() {
        initialiseNodes();
        layoutNodes();
    }

    private void initialiseNodes() {
        canvas = new Canvas();
        statusLabel = new Label();
        statusLabel.setStyle("-fx-background-color: grey");
    }

    private void layoutNodes() {
        canvas.setWidth(800.0);
        canvas.setHeight(600.0);

        statusLabel.setPadding(new Insets(5.0));
        statusLabel.setMaxWidth(Double.MAX_VALUE);

        canvas.getGraphicsContext2D().setFill(Color.WHITE);
        canvas.getGraphicsContext2D().fillRect(0.0, 0.0, 800.0, 600.0);

        setCenter(canvas);
        setBottom(statusLabel);
    }

    Canvas getCanvas() {
        return canvas;
    }

    Label getStatusLabel() {
        return statusLabel;
    }

    void drawDot(double x, double y) {
        canvas.getGraphicsContext2D().setFill(Color.BLUEVIOLET);
        canvas.getGraphicsContext2D().fillOval(
                x - DOT_RADIUS, y - DOT_RADIUS,
                2.0 * DOT_RADIUS, 2.0 * DOT_RADIUS);
    }
}
